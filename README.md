<img alt="BRABBEL" src="artwork/brabbel-logo.png"/>

BRABBEL
=======

(c) Markus Hoffmann 1997-2003, 2021

a universal text generator without neural networks.


inspired by an idea published in Spektrum der Wissenschaft

A first version was programmed in GfA-Basic for ATARI ST 1986

A very early version of Artificial Intelligence.


Description
-----------


Thats what ChatGPT (Dec2022) says about brabbel:

"... it seems that the "brabbel" program is a simple language model that uses 
character statistics to generate text based on a given input. Language models 
are computer programs that analyze and generate text, and they can be used for 
a variety of applications such as natural language processing, machine 
translation, and text generation.

The ChatGPT model [...] is a more advanced language model 
developed by OpenAI that is trained on a large dataset of human conversations. 
It uses a transformer neural network architecture and is able to generate 
coherent and contextually appropriate responses to user inputs.

In comparison, the "brabbel" program is a much simpler language model that 
uses a basic probabilistic model to generate text based on character 
frequencies. While it may not be as sophisticated as more modern language 
models like ChatGPT, it could still be a useful tool for generating text or 
for learning about the basic concepts of language modeling."


Input Text Collections
----------------------

If you need to have text input for this tool, why not try the full text
version of the work of William Shakespeare

Get it from project gutenberg:


<pre>
wget https://www.gutenberg.org/cache/epub/100/pg100.txt
</pre>


### Important Note:

    BRABBEL is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
    (Basically that means, free, open source, use and modify as you like, don't
    incorporate it into non-free software, no warranty of any sort, don't blame me
    if it doesn't work.)
    
    Please read the file INSTALL for compiling instructions.

