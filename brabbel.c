/* brabbel.c  a creative text processor (c) Markus Hoffmann */

/* This file is part of BRABBEL, the creative text processor 
 * ==========================================================
 * BRABBEL is free software and comes with NO WARRANTY - 
 * read the file COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#ifdef WINDOWS
void bzero(void *s, size_t n);
#define EX_OK 0
#define EX_USAGE 0
#define EX_NOINPUT 0
#define EX_CANTCREAT 0
#else
#include <sysexits.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>

#define FALSE    0
#define TRUE     (!FALSE)

#define VERSION "1.03"

char *prgname=NULL;
char **ifilenames=NULL;
int anzifile=0;
char seed[256];
int doseed=0;
int oc_stat[256];
char *textbuffer=NULL;
long textbufferlen=0;
char *outputbuf;
int intellig=4;
int alen=75;
int verbose=0;
   
static void usage() {
  printf("Usage: %s [-i -l -s] input.txt [input2.txt ...]\n\n",prgname);
  printf(" -i --intellig <number>\t--- IQ\t[%d]\n",intellig);
  printf(" -l --length <number>\t--- set length of the generated output\t[%d]\n",alen); 
  puts(  " -s --seed <text>\t--- set text seed\n"
         " -h --help\t--- show usage\n"
         " -v\t\t--- be more verbose\n"
         " -q\t\t--- be more quiet\n"
      );
}
static void intro(){
  puts("*****************************************************\n"
       "*          BRABBEL              V. " VERSION "             *\n"
       "*            by Markus Hoffmann 1997-2021 (c)       *\n"
       "*****************************************************\n");
}

/* Process command line parameters */

static void init(int anzahl, char *argumente[]) {
  int count,quitflag=0;
  for(count=1;count<anzahl;count++) {
    if(!strcmp(argumente[count],"-h") || !strcmp(argumente[count],"--help")) {
      intro();
      usage();
      quitflag=1;
    } 
    else if(!strcmp(argumente[count],"-i") || !strcmp(argumente[count],"--intellig"))  intellig=atoi(argumente[++count]);
    else if(!strcmp(argumente[count],"-l") || !strcmp(argumente[count],"--length"))  alen=atoi(argumente[++count]);
    else if(!strcmp(argumente[count],"-s") || !strcmp(argumente[count],"--seed"))  {doseed=1;strcpy(seed,argumente[++count]); }
    else if(!strcmp(argumente[count],"-v"))  verbose++;
    else if(!strcmp(argumente[count],"-q"))  verbose--;
    else if(*(argumente[count])=='-') ; /* do nothing*/
    else {
      ifilenames=realloc(ifilenames,sizeof(char *)*anzifile+1);
      ifilenames[anzifile++]=argumente[count];
    }
  }
  if(quitflag) exit(EX_OK);
}

static int irandom(int m) { return((random() & 0xffff)*m/0x10000); }

static int getbest(char *p){ 
  int i,sum=0,zu;

  for(i=0;i<256;i++) sum+=oc_stat[i];
	
  if(sum==0) return(0);

  zu=irandom(sum);	
  for(i=0;i<256;i++) {
    zu-=oc_stat[i];
    if(zu<0) {*p=(char)i;return(1);}
  }
  fprintf(stderr,"ERROR\n");
  return(-1);
}

static void dostat(char *old) {       
  int intellig=strlen(old);
  char *pos=strstr(textbuffer,old);
  while(pos && pos<textbuffer+textbufferlen) {
    oc_stat[(unsigned int)pos[intellig] & 0xff]++;
    pos=strstr(pos+1,old);
  }
}

/* Return TRUE if the file given by its filename exists */

static int exist(const char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(FALSE);
  return(TRUE);
}

/* return length of an open file */

static size_t lof(FILE *n) {
  long laenge,position;
  position=ftell(n);
  if(position==-1) {
    perror("lof");
    return(0);
  }
  if(fseek(n,0,SEEK_END)==0){
    laenge=ftell(n);
    if(laenge<0) perror("lof:ftell");
    if(fseek(n,position,0)<0) perror("lof:fseek"); 
    return(laenge);
  } else perror("lof:fseek");
  return(-1);
}


/* Add a (normal ASCII) file to the text buffer. */

static int add_file(char *filename) {
  if(!exist(filename)) return(-1);
  if(verbose>0) {printf("<-- %s ",filename);fflush(stdout);}
  FILE *dptr=fopen(filename,"r");
  size_t len=lof(dptr);
  if(verbose>0) {printf("%ld [",len);fflush(stdout);}
  textbuffer=realloc(textbuffer,textbufferlen+len+1);
  int j=fread(textbuffer+textbufferlen,1,len,dptr);  
  textbufferlen+=len;
  textbuffer[textbufferlen]=0;
  fclose(dptr);
  if(verbose>0) printf("] %d\n",j);
  return(0);
}

/* Clean the content of the text buffer by removing non-text charackters */

static void clean_buffer() {
  int i=0,j=0,flag=0;
  for(i=0;i<textbufferlen;i++) {
    if(isspace(textbuffer[i])) {
      if(!flag) textbuffer[j++]=' ';
      flag=1;
    } else {
      textbuffer[j++]=textbuffer[i];
      flag=0;
    }
  }
  textbuffer[j]=0;
  textbufferlen=j;
}


int main(int anzahl, char *argumente[]) {
  int i;
  int sum,ccc;
  char *p2;
  char *p;
  prgname=*argumente;

  if(anzahl<2) {intro(); usage(); return(0);} /* not enough parameters */
  init(anzahl, argumente);                    /* read and process the command line */
  if(anzifile>0) {
    int i;
    for(i=0;i<anzifile;i++) add_file(ifilenames[i]);
  } else exit(EX_OK);
  
  if(textbufferlen==0) exit(EX_OK);

  if(verbose>1) printf("Textbufferlen=%ld\n",textbufferlen);

  srandom(200); 
  
  clean_buffer(); /* Clean the content of the text buffer */
  
  /* prepare output buffer */
  
  outputbuf=malloc(alen+1);
  strcpy(outputbuf,seed);  /* Start with the seed. */
  p=outputbuf+strlen(seed);
  int intel=intellig;
  
  while(p<outputbuf+alen) {
    if(intel>strlen(outputbuf)) intel=strlen(outputbuf);
    p2=p-intel;
    bzero(oc_stat,sizeof(oc_stat));
    dostat(p2);
    sum=0;
    ccc=0;
    for(i=0;i<256;i++) {sum+=oc_stat[i]; if(oc_stat[i]>0) ccc++;}
    if(verbose>1) printf("Start = <%s> --> %d %d (%d)\n",p2,sum,ccc,intel);
//    if(ccc>5) {intellig++; best=getbest(); old[intellig-1]=best;old[intellig]=0; goto again;}
//    else if(ccc<=1 && intellig>3) {intellig--; goto again;}
    
    int ret=getbest(p);
    if(ret>0) {
      *++p=0;
      intel=intellig;
    } else {
      intel--;
      if(intel==0) {printf("ERROR: no match.\n");break;}
    }
  }
  puts(outputbuf);
  free(textbuffer);
  free(outputbuf);
}
