# Makefile for brabbel (c) Markus Hoffmann V.1.03

# This file is part of BRABBEL, the creative text processor 
# ==========================================================
# BRABBEL is free software and comes with NO WARRANTY - 
# read the file COPYING for details.

# Insert the defs for your machine

SHELL=/bin/sh

LIBNO=1.03
RELEASE=1
NAME=brabbel

# Directories
prefix=/usr
exec_prefix=${prefix}
datarootdir = ${prefix}/share

BINDIR=${exec_prefix}/bin
DATADIR=${datarootdir}
MANDIR=${datarootdir}/man


# Cross-Compiler fuer Windows-Excecutable
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
ARMCC=arm-linux-gcc

# Register variables (-ffixed-reg) -Wall
REGS=-fno-omit-frame-pointer  

# Optimization and debugging options
OPT=-O3

CC=gcc

CSRC= brabbel.c
HSRC= 
MANSRC= brabbel.1
DOC= README.md LICENSE 
DEBDOC= README.md

DIST= $(DOC) $(HSRC) $(CSRC) $(MANSRC) Makefile
BINDIST= $(DOC) brabbel $(MANSRC)

all: brabbel


brabbel: brabbel.c
	$(CC) $(OPT) -o $@ $< -Wall
brabbel.exe : brabbel.c
	$(WINCC) -DWINDOWS $(OPT) $(WINLINKFLAGS) -o $@ $< $(WINLIBS)
	strip $@
install : $(NAME) $(NAME).1
	install -s -m 755 $(NAME) $(BINDIR)/
	install -m 644 $(NAME).1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/man1/$(NAME).1

doc-pak: $(DEBDOC) Debian/changelog.Debian Debian/copyright
	mkdir -p $@
	cp $(DEBDOC) $@/
	cp Debian/changelog.Debian $@/
	gzip -n -9 $@/changelog.Debian
	cp Debian/copyright $@/

deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) \
	--pkgrelease $(RELEASE)  \
	--maintainer kollo@users.sourceforge.net \
	--backup \
	--pkggroup science \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	rm -f backup-*.tgz
	sudo chown 1000 $(NAME)_$(LIBNO)-$(RELEASE)_*.deb

clean :
	rm -f *.o a.out  backup-*.tgz 
distclean: clean
	rm -f brabbel brabbel.exe 
	rm -rf doc-pak

